program DelphiReact;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.JSON,
  Horse,
  Horse.Jhonson,
  Horse.CORS,
  ServerReact.Model.Connection in 'src\Model\ServerReact.Model.Connection.pas',
  ServerReact.Model.Entidades.Usuario in 'src\Model\Entidades\ServerReact.Model.Entidades.Usuario.pas',
  ServerReact.Model.DAOGeneric in 'src\Model\ServerReact.Model.DAOGeneric.pas',
  ServerReact.Controller.Usuario in 'src\Controller\ServerReact.Controller.Usuario.pas';

var
  App : THorse;

begin
  App := THorse.Create(9000);
  App.Use(Jhonson);
  App.Use(CORS);

  THorse.Get('/',
    procedure(Req: THorseRequest; Res: THorseResponse; Next: TProc)
    begin
      Res.Send('<h2>It'+ #39 + 's Work!</h2>');
    end);

  ServerReact.Controller.Usuario.Registry(App);

  App.Start;

end.

