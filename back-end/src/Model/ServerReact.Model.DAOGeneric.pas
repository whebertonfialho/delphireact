unit ServerReact.Model.DAOGeneric;

interface

uses
  System.JSON,
  REST.Json,
  SimpleInterface,
  SimpleDAO,
  SimpleAttributes,
  SimpleQueryFiredac,
  Data.DB,
  DataSetConverter4D,
  DataSetConverter4D.Impl,
  DataSetConverter4D.Helper,
  DataSetConverter4D.Util;

type

  iDAOGeneric<T : class> = interface
    ['{C4517202-DB1A-4922-84BF-BF2FFF1BA06D}']
    function Find : TJSONArray; overload;
    function Find (const aID : string ) : TJsonObject; overload;
    function Insert (const aJsonObject : TJsonObject) : TJsonObject;
    function Update (const aJsonObject : TJsonObject) : TJsonObject;
    function Delete (aField : String; aValue : String) : TJsonObject;
  end;

  TDAOGeneric<T : class, constructor> = class(TInterfacedObject, IDAOGeneric<T>)
  private
    FConn : iSimpleQuery;
    FDAO : iSimpleDAO<T>;
    FDataSource : TDataSource;
  public
    constructor Create;
    destructor Destroy; override;
    class function New : iDAOGeneric<T>;
    function Find : TJSONArray; overload;
    function Find (const aID : string ) : TJsonObject; overload;
    function Insert (const aJsonObject : TJsonObject) : TJsonObject;
    function Update (const aJsonObject : TJsonObject) : TJsonObject;
    function Delete (aField : String; aValue : String) : TJsonObject;
end;
implementation

{ TDAOGeneric<T> }

uses ServerReact.Model.Connection, System.SysUtils;

constructor TDAOGeneric<T>.Create;
begin
  FDataSource := TDataSource.Create(nil);
  ServerReact.Model.Connection.Connected;
  FConn := TSimpleQueryFiredac.New(ServerReact.Model.Connection.FConn);
  FDAO := TSimpleDAO<T>.New(FConn).DataSource(FDataSource);
end;

function TDAOGeneric<T>.Delete(aField, aValue: String): TJsonObject;
begin
  FDAO.Delete(aField, aValue);
  Result := FDataSource.DataSet.AsJSONObject;
end;

destructor TDAOGeneric<T>.Destroy;
begin
  FDataSource.Free;
  ServerReact.Model.Connection.Disconnected;
  inherited;
end;

function TDAOGeneric<T>.Find(const aID: string): TJsonObject;
begin
  FDAO.Find(StrToInt(aId));
  Result := FDataSource.DataSet.AsJSONObject;
end;

function TDAOGeneric<T>.Find: TJSONArray;
begin
  FDAO.Find;
  Result := FDataSource.DataSet.AsJSONArray;
end;

class function TDAOGeneric<T>.New: iDAOGeneric<T>;
begin
  Result := Self.Create;
end;

function TDAOGeneric<T>.Insert(const aJsonObject: TJsonObject): TJsonObject;
begin
  FDAO.Insert(TJson.JsonToObject<T>(aJsonObject));
  Result := FDataSource.DataSet.AsJSONObject;
end;

function TDAOGeneric<T>.Update(const aJsonObject: TJsonObject): TJsonObject;
begin
   FDAO.Update(TJson.JsonToObject<T>(aJsonObject));
   Result := FDataSource.DataSet.AsJSONObject;
end;

end.
