unit ServerReact.Model.Entidades.Usuario;

interface

uses
  SimpleAttributes;

type
  [Tabela('Usuario')]
  TUsuario = class
    private
    FID: Integer;
    FSTATUS: Integer;
    FSENHA: String;
    FNOME: String;
    procedure SetID(const Value: Integer);
    procedure SetNOME(const Value: String);
    procedure SetSENHA(const Value: String);
    procedure SetSTATUS(const Value: Integer);
    public
      constructor Create;
      destructor Destroy; override;
    published
      [Campo('ID'), PK]
      property ID : Integer read FID write SetID;
      [Campo('NOME')]
      property NOME : String read FNOME write SetNOME;
      [Campo('SENHA')]
      property SENHA : String read FSENHA write SetSENHA;
      [Campo('STATUS')]
      property STATUS : Integer read FSTATUS write SetSTATUS;
  end;

implementation

{ TUsuario }

constructor TUsuario.Create;
begin

end;

destructor TUsuario.Destroy;
begin

  inherited;
end;

procedure TUsuario.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TUsuario.SetNOME(const Value: String);
begin
  FNOME := Value;
end;

procedure TUsuario.SetSENHA(const Value: String);
begin
  FSENHA := Value;
end;

procedure TUsuario.SetSTATUS(const Value: Integer);
begin
  FSTATUS := Value;
end;

end.
