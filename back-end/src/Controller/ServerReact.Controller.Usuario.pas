unit ServerReact.Controller.Usuario;

interface

uses
  Horse,
  System.JSON,
  ServerReact.Model.DAOGeneric,
  ServerReact.Model.Entidades.Usuario;

procedure Registry(App : THorse);
procedure Get(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure GetId(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure Insert(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure Update(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure Delete(Req: THorseRequest; Res: THorseResponse; Next: TProc);

implementation

procedure Registry(App : THorse);
begin
  App.Get('/usuario', Get);
  App.Get('/usuario/:id', GetId);
  App.Post('/usuario', Insert);
  App.Put('/usuario', Update);
  App.Delete('/usuario/:id', Delete);
end;

procedure Get(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  FDAO :  iDAOGeneric<TUsuario>;
begin
  FDAO := TDAOGeneric<TUsuario>.New;
  Res.Send<TJsonArray>(FDAO.Find);
end;

procedure GetId(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  FDAO :  iDAOGeneric<TUsuario>;
begin
  FDAO := TDAOGeneric<TUsuario>.New;
  Res.Send<TJsonObject>(FDAO.Find(Req.Params.Items['id']));
end;

procedure Insert(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  FDAO : iDAOGeneric<TUsuario>;
begin
  FDAO := TDAOGeneric<TUsuario>.New;
  Res.Send<TJsonObject>(FDAO.Insert(Req.Body<TJsonObject>));
end;

procedure Update(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  FDAO : iDAOGeneric<TUsuario>;
begin
  FDAO := TDAOGeneric<TUsuario>.New;
  Res.Send<TJsonObject>(FDAO.Update(Req.Body<TJsonObject>));
end;

procedure Delete(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  FDAO : iDAOGeneric<TUsuario>;
begin
  FDAO := TDAOGeneric<TUsuario>.New;
  Res.Send<TJsonObject>(FDAo.Delete('ID', Req.Params.Items['id']));
end;

end.
