import React from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import UsuarioForm from './components/usuario/UsuarioForm'
import UsuarioTable from './components/usuario/UsuarioTable'

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={UsuarioTable} />
            <Route exact path='/usuarios' component={UsuarioTable} />
            <Route path='/usuarios/:id' component={UsuarioForm} />
        </Switch>
    </BrowserRouter>
)

export default Routes;