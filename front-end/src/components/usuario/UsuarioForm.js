import React, { Component } from "react";
import api from "../../services/api";

class UsuarioForm extends Component{
    
    state = {
        _usuario : {}
    }
    
    async componentDidMount(){
        const { id } = this.props.match.params;
        const response = api.get(`/usuario/${id}`);
        this.setState({ _usuario: response.data });
    }

    render(){

        const _usuario = this.state;

        return(
            <div className="col-sm-12">
               <form>
                    <div className="mb-3">
                        <label for="id" className="form-label">ID</label>
                        <input type="text" value={_usuario.ID} className="form-control" id="id" />
                    </div>
                    <div className="mb-3">
                        <label for="usuario" className="form-label">Usuário</label>
                        <input type="text" className="form-control" id="usuario" />
                    </div>
                    <button type="submit" value={_usuario.NOME} className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default UsuarioForm;