import React, { Component } from "react";
import api from "../../services/api";

class UsuarioTable extends Component{
    
    state = {
        _usuarios : []
    }

    async componentDidMount(){
        const response = await api.get('usuario');
        this.setState({ _usuarios: response.data })
    }
    
    render(){
        return(
           <div className="container">
               <br></br>
               <h3>Lista de Usuarios ({this.state._usuarios.length})</h3>
               <br></br>
               <table className="table table-hover">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.state._usuarios.map( usuario => (
                            <tr key={usuario.ID}>
                                <th scope="row" >{usuario.ID}</th>
                                <td>{usuario.NOME}</td>
                                <td>{usuario.STATUS}</td>
                            </tr> 
                        )) }
                        
                    </tbody>
                </table>
           </div>
        )
    }
}

export default UsuarioTable;